Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/mhx/dwarfs
Upstream-Name: dwarfs
Upstream-Contact: https://github.com/mhx/dwarfs/issues
Files-Excluded:
 zstd
 xxHash
 parallel-hashmap
Comment: Replace vendored libraries with packaged counterparts.

Files: *
Copyright: 2020-2024 Marcus Holland-Moritz <github@mhxnet.de>
License: GPL-3+

Files: debian/*
Copyright:
 2024 Alex Myczko <tar@debian.org>
 2024 Maytham Alsudany <maytha8thedev@gmail.com>
License: GPL-3+
Comment: Debian packaging is licensed under the same terms as upstream.

Files: fsst/*
Copyright:
 2018-2020 CWI
 2018-2020 TU Munich
 2018-2020 FSU Jena
License: Expat

Files:
 folly/*
 fbthrift/*
Copyright: 2019-2024 Meta Platforms, Inc. and affiliates.
License: Apache-2.0

Files:
 folly/build/fbcode_builder/*
 fbthrift/build/fbcode_builder/*
Copyright: 2019-2024 Meta Platforms, Inc. and affiliates.
License: Expat

Files: fbthrift/thrift/compiler/detail/mustache/*
Copyright:
 2019-2024 Meta Platforms, Inc. and affiliates.
 2015 Daniel Sipka
License: Expat

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public License version
 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
Comment:
 On Debian systems, the full text of the Apache license, version 2.0 can be
 found at /usr/share/common-licenses/Apache-2.0

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
